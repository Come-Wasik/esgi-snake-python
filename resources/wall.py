from pygame.math import Vector2
import pygame
import random


class WALL():
    def __init__(self, screen, cell_size, cell_number):
        self.screen = screen
        self.cell_size = cell_size
        self.cell_number = cell_number
        # self.color = (193, 63, 0)
        self.randomizePosition()
        self.picture = pygame.image.load("sprites/wall.jpg").convert_alpha()

    def draw(self):
        wall_rect = pygame.Rect(int(self.pos.x * self.cell_size), int(self.pos.y * self.cell_size), self.cell_size, self.cell_size)
        # pygame.draw.rect(self.screen, self.color, wall_rect)
        self.screen.blit(self.picture, wall_rect)

    """Spawn the wall at a random position"""

    def randomizePosition(self):
        self.x = random.randint(0, self.cell_number-1)
        self.y = random.randint(0, self.cell_number-1)
        self.pos = Vector2(self.x, self.y)
