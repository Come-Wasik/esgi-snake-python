import pygame
import sys
from resources.snake import SNAKE
from resources.fruit import FRUIT
from resources.wall import WALL
from resources.wormhole import WORMHOLE
from pygame.math import Vector2
import os.path
import pygame.gfxdraw


class GAME:
    """Init the game elements"""

    def __init__(self, screen, cell_size, cell_number, game_font):
        self.screen = screen
        self.cell_size = cell_size
        self.cell_number = cell_number
        self.snake = SNAKE(self.screen, self.cell_size)
        self.fruit = FRUIT(self.screen, self.cell_size, self.cell_number)
        self.walls = []
        self.wormholes = []
        self.eated_fruit = 0
        self.highscore = self.get_highscore()
        self.game_font = game_font
        self.wall_count = 3
        self.wormhole_count = 2

        self.place_fruit_on_empty_cell()
        self.place_walls_on_empty_cell()
        self.place_wormholes_on_empty_cell()

    # Main game logic
    def loop_step(self):
        SCREEN_UPDATE = pygame.USEREVENT

        for event in pygame.event.get():
            # If the player clicks the close button, quit the game
            if event.type == pygame.QUIT:
                self.stop()
            if event.type == SCREEN_UPDATE:
                self.screen_update()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.stop()
                # If the user press a direction key, move the snake in that direction
                if event.key == pygame.K_DOWN:
                    self.snake.head_down()
                if event.key == pygame.K_UP:
                    self.snake.head_up()
                if event.key == pygame.K_LEFT:
                    self.snake.head_left()
                if event.key == pygame.K_RIGHT:
                    self.snake.head_right()

        # Fill the screen with a solid colour
        self.screen.fill((175, 215, 70))
        self.draw_game()

    """Draw the game drawings, texts and sprites on the screen"""

    def draw_game(self):
        self.draw_grass()
        self.snake.draw_green()
        self.fruit.draw()
        for wall in self.walls:
            wall.draw()
        for wormholeCouple in self.wormholes:
            wormholeCouple[0].draw()
            wormholeCouple[1].draw()
        self.draw_score()

    """Update the game loop"""

    def screen_update(self):
        if self.snake.isMoving():
            self.snake.move_snake()
            if self.check_collision_with_screen():
                self.teleport_snake_on_other_side()
            if self.check_collision_with_wormholes():
                self.teleport_snake_to_wormhole_exit()
            if self.check_collision_with_fruit():
                self.snake.grow_up()
                self.eated_fruit += 1
                self.snake.play_crunch_sound()
                # Avoid the snake to eat the fruit when repositionning it
                self.place_fruit_on_empty_cell()
            if self.check_collision_with_self():
                self.game_over()
            if self.check_collision_with_wall():
                self.game_over()

    """Stop pygame then close the application properly"""

    def stop(self):
        pygame.quit()
        sys.exit()

    """The ending scenario"""

    def game_over(self):
        self.register_hightscore()
        self.snake.restart_position()
        self.place_fruit_on_empty_cell()
        self.eated_fruit = 0

    # Collision managment
    def check_collision_with_fruit(self):
        return self.snake.body[0] == self.fruit.pos

    # Screen collision managment
    def check_collision_with_screen(self):
        return not 0 <= self.snake.body[0].x < self.cell_number or not 0 <= self.snake.body[0].y < self.cell_number

    def get_collision_side(self) -> str:
        # Get the text left, right, top or bottom which what the snake is colliding with
        if self.snake.body[0].x < 0:
            return "left"
        elif self.snake.body[0].x > self.cell_number - 1:
            return "right"
        elif self.snake.body[0].y < 0:
            return "top"
        elif self.snake.body[0].y > self.cell_number - 1:
            return "bottom"

    def check_collision_with_wormholes(self):
        return self.position_colide_with_wormholes(self.snake.body[0])

    def check_collision_with_self(self):
        return self.snake.body[0] in self.snake.body[1:]

    def check_collision_with_wall(self):
        for wall in self.walls:
            if self.snake.body[0] == wall.pos:
                return True

    def draw_grass(self):
        grass_color = (167, 209, 61)
        for x in range(self.cell_number):
            if x % 2 == 0:
                for y in range(self.cell_number):
                    if y % 2 == 0:
                        grass_rect = pygame.Rect(
                            x * self.cell_size, y * self.cell_size, self.cell_size, self.cell_size)
                        pygame.draw.rect(self.screen, grass_color, grass_rect)
            else:
                for y in range(self.cell_number):
                    if y % 2 != 0:
                        grass_rect = pygame.Rect(
                            x * self.cell_size, y * self.cell_size, self.cell_size, self.cell_size)
                        pygame.draw.rect(self.screen, grass_color, grass_rect)

    def draw_score(self):
        # Define colors
        text_color = (56, 74, 12)
        bg_color = (167, 209, 61, 152)

        # Create the text and render it into the game font
        score_text = "Score : " + str(self.eated_fruit)
        highscore_text = "Best score : " + str(self.highscore)
        score_surface = self.game_font.render(score_text, True, text_color)
        highscore_surface = self.game_font.render(
            highscore_text, True, text_color)

        # Get fruit picture
        fruit_picture = self.fruit.getPicture()

        # Create the position elements
        score_x = int(self.cell_number * self.cell_size - 60)
        score_y = int(self.cell_size * self.cell_number - 80)
        score_rect = score_surface.get_rect(center=(score_x, score_y))
        apple_rect = fruit_picture.get_rect(
            midright=(score_rect.left, score_rect.centery))
        highscore_rect = highscore_surface.get_rect(
            center=(score_x - 20, score_y + score_rect.height))
        bg_rect = pygame.Rect(apple_rect.left - 10, apple_rect.top - 6,
                              apple_rect.width + score_rect.width + 18, apple_rect.height + highscore_rect.height + 12)

        # Draw the background rectangle
        # pygame.draw.rect(self.screen, bg_color, bg_rect)
        pygame.gfxdraw.box(self.screen, bg_rect, bg_color)

        # Draw the score text
        self.screen.blit(score_surface, score_rect)
        self.screen.blit(fruit_picture, apple_rect)
        self.screen.blit(highscore_surface, highscore_rect)
        pygame.draw.rect(self.screen, text_color, bg_rect, 2)

    def place_fruit_on_empty_cell(self):
        # Avoid the snake to eat the fruit when repositionning it
        while self.fruit.pos in self.snake.body or self.fruit.pos in self.walls or self.position_colide_with_wormholes(self.fruit.pos):
            self.fruit.randomizePosition()

    def place_walls_on_empty_cell(self):
        for i in range(self.wall_count):
            wall = WALL(self.screen, self.cell_size, self.cell_number)
            while wall.pos in self.snake.body or wall.pos == self.fruit.pos or wall.pos in self.walls or self.position_colide_with_wormholes(wall.pos):
                wall.randomizePosition()
            self.walls.append(wall)

    def place_wormholes_on_empty_cell(self):
        for i in range(self.wormhole_count):
            hole1 = WORMHOLE(self.screen, self.cell_size, self.cell_number)
            while hole1.pos in self.snake.body or hole1.pos == self.fruit.pos or hole1.pos in self.walls or self.position_colide_with_wormholes(hole1.pos):
                hole1.randomizePosition()
            hole2 = WORMHOLE(self.screen, self.cell_size, self.cell_number)
            while hole2.pos in self.snake.body or hole2.pos == self.fruit.pos or hole2.pos in self.walls or hole2 == hole1 or self.position_colide_with_wormholes(hole2.pos):
                hole2.randomizePosition()
            self.wormholes.append([hole1, hole2])

    def register_hightscore(self):
        best_score = self.get_highscore()
        if self.eated_fruit > best_score:
            with open("best_score.txt", "w") as f:
                f.write(str(self.eated_fruit))
            self.highscore = str(self.eated_fruit)

    def get_highscore(self) -> int:
        if os.path.exists("best_score.txt"):
            with open("best_score.txt", "r") as f:
                best_score = int(f.read())
        else:
            best_score = 0
        return best_score

    def teleport_snake_on_other_side(self):
        screen_side = self.get_collision_side()
        if screen_side == "top":
            # If the snake is colliding with the top of the screen, move it from the bottom
            self.snake.body[0] = Vector2(
                self.snake.body[0].x, self.cell_number - 1)
        elif screen_side == "bottom":
            # If the snake is colliding with the bottom of the screen, move it from the top
            self.snake.body[0] = Vector2(self.snake.body[0].x, 0)
        elif screen_side == "left":
            # If the snake is colliding with the left of the screen, move it from the right
            self.snake.body[0] = Vector2(
                self.cell_number - 1, self.snake.body[0].y)
        elif screen_side == "right":
            # If the snake is colliding with the right of the screen, move it from the left
            self.snake.body[0] = Vector2(0, self.snake.body[0].y)

    def teleport_snake_to_wormhole_exit(self):
        for couple in self.wormholes:
            if self.snake.body[0] == couple[0].pos:
                self.snake.body[0] = couple[1].pos
            elif self.snake.body[0] == couple[1].pos:
                self.snake.body[0] = couple[0].pos

    """Analyse if any wormhole colide with a position"""
    def position_colide_with_wormholes(self, position: Vector2) -> bool:
        for couple in self.wormholes:
            if position == couple[0].pos:
                return True
            elif position == couple[1].pos:
                return True
        return False