from pygame.math import Vector2
import pygame
import random

class FRUIT:
    def __init__(self, screen, cell_size, cell_number):
        self.screen = screen
        self.cell_size = cell_size
        self.cell_number = cell_number
        self.randomizePosition()
        self.picture = pygame.image.load("sprites/apple.png").convert_alpha()

    """Draw the fruit on the screen"""

    def draw(self):
        fruit_rect = pygame.Rect(int(self.pos.x * self.cell_size), int(self.pos.y * self.cell_size), self.cell_size, self.cell_size)
        self.screen.blit(self.picture, fruit_rect)
        # pygame.draw.rect(self.screen, (126, 166, 144), fruit_rect)

    """Spawn the fruit at a random position"""

    def randomizePosition(self):
        self.pos = Vector2(random.randint(0, self.cell_number-1),random.randint(0, self.cell_number-1))

    def getPicture(self):
        return self.picture
