from pygame.math import Vector2
import pygame

class SNAKE:
    """Init the snake body (the blocks on the screen) and the snake future direction"""

    def __init__(self, screen, cell_size, ini_body, ini_direction: Vector2):
        self.screen = screen
        self.cell_size = cell_size
        self.crunch_sound = pygame.mixer.Sound("sounds/apple_crunch.wav")
        self.headColor = (0, 193, 23)
        self.bodyColor = (0, 223, 23)
        self.isDead = False
        # Setup the snake elements on the grid
        self.ini_body = ini_body
        self.ini_direction = ini_direction
        self.reset_positions()

    def reset_positions(self):
        self.body = self.ini_body.copy()
        self.direction = self.ini_direction.copy()

    """Set the snake color"""
    def setColor(self, headColor, bodyColor):
        self.headColor = headColor
        self.bodyColor = bodyColor

    """Draw the green snake"""

    def draw(self):
        for block in self.body:
            x_pos = int(block.x * self.cell_size)
            y_pos = int(block.y * self.cell_size)
            block_rect = pygame.Rect(x_pos, y_pos, self.cell_size, self.cell_size)
            if block != self.body[0]:
                snake_color = self.bodyColor
            else:
                snake_color = self.headColor
            pygame.draw.rect(self.screen, snake_color, block_rect)

    """Move the snake in a specific direction"""

    def move_snake(self):
        body_copy = self.body[:-1]
        body_copy.insert(0, self.body[0] + self.direction)
        self.body = body_copy.copy()

    # The following move the snake in a specific direction
    def head_up(self):
        if self.body[1].y >= self.head.y:
            self.direction = Vector2(0, -1)

    def head_down(self):
        if self.body[1].y <= self.head.y:
            self.direction = Vector2(0, 1)

    def head_left(self):
        # If the direction x is 0 and the snake is moving left, the snake will not move or if the snake body[1] is not lower than the snake body[0]
        if self.body[1].x >= self.head.x:
            self.direction = Vector2(-1, 0)

    def head_right(self):
        if self.body[1].x <= self.head.x:
            self.direction = Vector2(1, 0)

    def grow_up(self):
        self.body.append(self.body[-1] + self.direction)

    def play_crunch_sound(self):
        self.crunch_sound.play(1, 800, 0)

    def isMoving(self):
        return self.direction != Vector2(0, 0)

    def die(self):
        self.isDead = True

    def resurrect(self):
        self.isDead = False

    def is_alive(self):
        return not self.isDead

    def get_head(self):
        return self.body[0]

    def set_head(self, new_head):
        self.body[0] = new_head

    head = property(get_head, set_head)