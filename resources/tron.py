import pygame
import sys
from resources.snake import SNAKE
from resources.fruit import FRUIT
from resources.wall import WALL
from resources.wormhole import WORMHOLE
from pygame.math import Vector2
import os.path
import pygame.gfxdraw


class GAME:
    """Init the game elements"""

    def __init__(self, screen, cell_size, cell_number, game_font):
        # Define arg values
        self.screen = screen
        self.cell_size = cell_size
        self.cell_number = cell_number
        self.game_font = game_font

        # Important logic data
        self.wall_count = 3
        self.wormhole_count = 2
        self.eated_fruit = 0  #  Score
        self.highscore = self.get_highscore()

        # Define snakes and init their positions in memory
        self.snakes = []
        snake_default_direction = Vector2(0, 0)
        # Position of each blocks of the snake is named a body
        snake_body_player_1 = [Vector2(5+i, 10) for i in range(3, 0, -1)]
        snake_body_player_2 = [Vector2(3+i, 3) for i in range(3)]
        self.snakes.append(SNAKE(self.screen, self.cell_size,
                           snake_body_player_1, snake_default_direction))
        self.snakes.append(SNAKE(self.screen, self.cell_size,
                           snake_body_player_2, snake_default_direction))

        # Set the snake colors
        self.player1().setColor((0, 193, 23), (0, 223, 23))
        self.player2().setColor((193, 23, 0), (223, 53, 0))

        # Define fruit, walls, worm_holes then place them in a random position in memory
        self.fruit = None
        self.walls = []
        self.wormholes = []
        self.place_fruit_on_empty_cell()
        self.place_walls_on_empty_cell()
        self.place_wormholes_on_empty_cell()

    # Main game logic
    def loop_step(self):
        SCREEN_UPDATE = pygame.USEREVENT

        for event in pygame.event.get():
            # If the player clicks the close button, quit the game
            if event.type == pygame.QUIT:
                self.stop()
            if event.type == SCREEN_UPDATE:
                self.screen_update()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.stop()

                if self.player1().is_alive():
                    # Head the player 1 in a direction by using the keys down, up, left and right
                    if event.key == pygame.K_DOWN:
                        self.player1().head_down()
                    if event.key == pygame.K_UP:
                        self.player1().head_up()
                    if event.key == pygame.K_LEFT:
                        self.player1().head_left()
                    if event.key == pygame.K_RIGHT:
                        self.player1().head_right()

                if self.player2().is_alive():
                    # Head the player 2 in a direction by using the keys k, i, j and l
                    if event.key == pygame.K_k:
                        self.player2().head_down()
                    if event.key == pygame.K_i:
                        self.player2().head_up()
                    if event.key == pygame.K_j:
                        self.player2().head_left()
                    if event.key == pygame.K_l:
                        self.player2().head_right()

        # Fill the screen with a solid colour
        self.screen.fill((175, 215, 70))
        self.draw_game()

    """Draw the game drawings, texts and sprites on the screen"""

    def draw_game(self):
        self.draw_grass()
        [snake.draw() for snake in self.snakes if snake.is_alive()]
        self.fruit.draw()
        [wall.draw() for wall in self.walls]
        for wormholeCouple in self.wormholes:
            wormholeCouple[0].draw()
            wormholeCouple[1].draw()
        self.draw_score()

    """Update the game loop"""

    def screen_update(self):
        for snake in self.snakes:
            if snake.isMoving():
                snake.move_snake()
                if self.check_collision_with_screen(snake):
                    self.teleport_snake_on_other_side(snake)
                if self.check_collision_with_wormholes(snake):
                    self.teleport_snake_to_wormhole_exit(snake)
                if self.check_collision_with_fruit(snake):
                    snake.grow_up()
                    self.eated_fruit += 1
                    snake.play_crunch_sound()
                    # Avoid the snake to eat the fruit when repositionning it
                    self.place_fruit_on_empty_cell()
                if self.check_collision_with_self(snake):
                    snake.die()
                if self.check_collision_with_other_snake(snake):
                    touchedSnake = self.get_touched_snake(snake)
                    touchedSnake.die()
                if self.check_collision_with_wall(snake):
                    snake.die()

        # If all the snakes are dead, the game is over
        if all([not snake.is_alive() for snake in self.snakes]):
            self.game_over()

    """Stop pygame then close the application properly"""

    def stop(self):
        pygame.quit()
        sys.exit()

    """The ending scenario"""

    def game_over(self):
        self.register_hightscore()
        for player in self.snakes:
            player.reset_positions()
            player.resurrect()
        self.place_fruit_on_empty_cell()
        self.eated_fruit = 0

    # Collision managment
    def check_collision_with_fruit(self, snake: SNAKE) -> bool:
        return snake.head == self.fruit.pos

    # Screen collision managment
    def check_collision_with_screen(self, snake: SNAKE) -> bool:
        return not 0 <= snake.head.x < self.cell_number or not 0 <= snake.head.y < self.cell_number

    def get_collision_side(self, snake: SNAKE) -> str:
        # Get the text left, right, top or bottom which what the snake is colliding with
        if snake.head.x < 0:
            return "left"
        elif snake.head.x > self.cell_number - 1:
            return "right"
        elif snake.head.y < 0:
            return "top"
        elif snake.head.y > self.cell_number - 1:
            return "bottom"

    def check_collision_with_wormholes(self, snake: SNAKE) -> bool:
        return self.position_collide_with_wormholes(snake.head)

    def check_collision_with_self(self, snake: SNAKE) -> bool:
        return snake.head in snake.body[1:]

    def check_collision_with_other_snake(self, snake: SNAKE) -> bool:
        return any([snake.head in other_snake.body
                    for other_snake in self.snakes
                    if other_snake != snake and other_snake.is_alive()])

    def get_touched_snake(self, snake: SNAKE) -> SNAKE:
        for other_snake in self.snakes:
            if other_snake != snake and other_snake.is_alive():
                if snake.head in other_snake.body:
                    return other_snake

    def check_collision_with_wall(self, snake: SNAKE):
        for wall in self.walls:
            if snake.head == wall.pos:
                return True

    def draw_grass(self):
        grass_color = (167, 209, 61)
        for x in range(self.cell_number):
            if x % 2 == 0:
                for y in range(self.cell_number):
                    if y % 2 == 0:
                        grass_rect = pygame.Rect(
                            x * self.cell_size, y * self.cell_size, self.cell_size, self.cell_size)
                        pygame.draw.rect(self.screen, grass_color, grass_rect)
            else:
                for y in range(self.cell_number):
                    if y % 2 != 0:
                        grass_rect = pygame.Rect(
                            x * self.cell_size, y * self.cell_size, self.cell_size, self.cell_size)
                        pygame.draw.rect(self.screen, grass_color, grass_rect)

    def draw_score(self):
        # Define colors
        text_color = (56, 74, 12)
        bg_color = (167, 209, 61, 152)

        # Create the text and render it into the game font
        score_text = "Score : " + str(self.eated_fruit)
        highscore_text = "Best score : " + str(self.highscore)
        score_surface = self.game_font.render(score_text, True, text_color)
        highscore_surface = self.game_font.render(
            highscore_text, True, text_color)

        # Get fruit picture
        fruit_picture = self.fruit.getPicture()

        # Create the position elements
        score_x = int(self.cell_number * self.cell_size - 60)
        score_y = int(self.cell_size * self.cell_number - 80)
        score_rect = score_surface.get_rect(center=(score_x, score_y))
        apple_rect = fruit_picture.get_rect(
            midright=(score_rect.left, score_rect.centery))
        highscore_rect = highscore_surface.get_rect(
            center=(score_x - 20, score_y + score_rect.height))
        bg_rect = pygame.Rect(apple_rect.left - 10, apple_rect.top - 6,
                              apple_rect.width + score_rect.width + 18, apple_rect.height + highscore_rect.height + 12)

        # Draw the background rectangle
        # pygame.draw.rect(self.screen, bg_color, bg_rect)
        pygame.gfxdraw.box(self.screen, bg_rect, bg_color)

        # Draw the score text
        self.screen.blit(score_surface, score_rect)
        self.screen.blit(fruit_picture, apple_rect)
        self.screen.blit(highscore_surface, highscore_rect)
        pygame.draw.rect(self.screen, text_color, bg_rect, 2)

    def place_fruit_on_empty_cell(self):
        fruit = FRUIT(self.screen, self.cell_size, self.cell_number)
        # Avoid the snake to eat the fruit when repositionning it
        while self.position_collide_with_snakes(fruit.pos) or fruit.pos in self.walls or self.position_collide_with_wormholes(fruit.pos):
            fruit.randomizePosition()
        self.fruit = fruit

    def place_walls_on_empty_cell(self):
        for i in range(self.wall_count):
            wall = WALL(self.screen, self.cell_size, self.cell_number)
            while self.position_collide_with_snakes(wall.pos) or wall.pos == self.fruit.pos or wall.pos in self.walls or self.position_collide_with_wormholes(wall.pos):
                wall.randomizePosition()
            self.walls.append(wall)

    def place_wormholes_on_empty_cell(self):
        for i in range(self.wormhole_count):
            hole1 = WORMHOLE(self.screen, self.cell_size, self.cell_number)
            while self.position_collide_with_snakes(hole1.pos) or hole1.pos == self.fruit.pos or hole1.pos in self.walls or self.position_collide_with_wormholes(hole1.pos):
                hole1.randomizePosition()
            hole2 = WORMHOLE(self.screen, self.cell_size, self.cell_number)
            while self.position_collide_with_snakes(hole2.pos) or hole2.pos == self.fruit.pos or hole2.pos in self.walls or hole2 == hole1 or self.position_collide_with_wormholes(hole2.pos):
                hole2.randomizePosition()
            self.wormholes.append([hole1, hole2])

    def register_hightscore(self):
        best_score = self.get_highscore()
        if self.eated_fruit > best_score:
            with open("best_score.txt", "w") as f:
                f.write(str(self.eated_fruit))
            self.highscore = str(self.eated_fruit)

    def get_highscore(self) -> int:
        if os.path.exists("best_score.txt"):
            with open("best_score.txt", "r") as f:
                best_score = int(f.read())
        else:
            best_score = 0
        return best_score

    def teleport_snake_on_other_side(self, snake: SNAKE):
        screen_side = self.get_collision_side(snake)
        if screen_side == "top":
            # If the snake is colliding with the top of the screen, move it from the bottom
            snake.head = Vector2(
                snake.head.x, self.cell_number - 1)
        elif screen_side == "bottom":
            # If the snake is colliding with the bottom of the screen, move it from the top
            snake.head = Vector2(snake.head.x, 0)
        elif screen_side == "left":
            # If the snake is colliding with the left of the screen, move it from the right
            snake.head = Vector2(
                self.cell_number - 1, snake.head.y)
        elif screen_side == "right":
            # If the snake is colliding with the right of the screen, move it from the left
            snake.head = Vector2(0, snake.head.y)

    def teleport_snake_to_wormhole_exit(self, snake: SNAKE):
        for couple in self.wormholes:
            if snake.head == couple[0].pos:
                snake.head = couple[1].pos
            elif snake.head == couple[1].pos:
                snake.head = couple[0].pos

    """Analyse if any wormhole colide with a position"""

    def position_collide_with_wormholes(self, position: Vector2) -> bool:
        for couple in self.wormholes:
            if position == couple[0].pos:
                return True
            elif position == couple[1].pos:
                return True
        return False

    def position_collide_with_snakes(self, position: Vector2) -> bool:
        return any([position in snake.body for snake in self.snakes if snake.is_alive()])

    """Helper for player 1"""

    def player1(self) -> SNAKE:
        return self.snakes[0]

    """Helper for player 2"""

    def player2(self) -> SNAKE:
        return self.snakes[1]
