import pygame
from resources.tron import GAME

cell_size = 40
cell_number = 15
step_in_ms = 100
framerate = 60
app_name = "Snake"


# Is the beginning of eveything
pygame.mixer.pre_init(44100, -16, 2, 512)
pygame.init()
game_font = pygame.font.Font("fonts/SUNMORI.otf", 25)

# Set the application name
pygame.display.set_caption(app_name)

# Create the window object
screen = pygame.display.set_mode(
    (cell_size * cell_number, cell_size * cell_number))
# Create a timer object to restrict the frame rate
clock = pygame.time.Clock()

# Create a new game logic instance
game_logic = GAME(screen, cell_size, cell_number, game_font)

# Setup the snake movement speed
SCREEN_UPDATE = pygame.USEREVENT
pygame.time.set_timer(SCREEN_UPDATE, step_in_ms)

# Game loop
while True:
    # Run the game logic for each frame
    game_logic.loop_step()

    # Display at screen all the elements displayed in memory. That also means keep the game running
    pygame.display.update()

    # Limit the framerate to 60 frames per second
    clock.tick(framerate)
